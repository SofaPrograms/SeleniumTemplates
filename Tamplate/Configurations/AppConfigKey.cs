﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamplate.Configurations
{
    public class AppConfigKey
    {
        public const string Browser = "Browser";
        public const string Username = "Username";
        public const string Password = "Password";
        public const string Website = "Website";
        public const string Log = "Log";
    }
}
