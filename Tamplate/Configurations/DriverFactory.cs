﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
 
namespace Tamplate.Configurations
{
    public  class DriverFactory
    {
        private static ChromeOptions GetChromeOptions()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArgument("start-maximized");
            return option;
        }

        public static IWebDriver CreateWebdriver()
        {
            IWebDriver Driver=null;

            var browser = new AppConfigReader().GetBrowser();

            if (browser == BrowserTypes.Chrome)
            {
             Driver =  new ChromeDriver(GetChromeOptions());
                
            }
            if (browser == BrowserTypes.Firefox)
            {
              Driver = new  FirefoxDriver();
               
            }
            if (browser == BrowserTypes.IExplorer)
            {
              Driver = new InternetExplorerDriver();
                
            }
            return Driver;
        }

    }
}
