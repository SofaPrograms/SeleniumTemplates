﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamplate.Configurations
{
   public static class UserFactory
    {
        public static User LastGeneratedUser;

        public static void Initialize()
        {
            LastGeneratedUser = null;
        }

        public static User Generate()
        {
            var user = new User
            {
               // EmailAddress = EmailAddressGenerator.Generate(),
              //  Password = PasswordGenerator.Generate()
            };

            LastGeneratedUser = user;
            return user;
        }
    }

    public class User
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}

