﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Tamplate.Configurations;

namespace Tamplate.Configurations
{
    public class AppConfigReader
    {
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);

            }
        }
        public  BrowserTypes GetBrowser()
        {
            string browser = ConfigurationManager.AppSettings.Get(AppConfigKey.Browser);
            return (BrowserTypes)Enum.Parse(typeof(BrowserTypes), browser);
        }
        public  string GetUsername()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKey.Username);
        }
        public   string GetPassword()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKey.Password);
        }
        public   string GetWebsite()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKey.Website);
        }
         public string GetLog()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKey.Log);
        }
    }
}
