﻿ 
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Tamplate.Configurations;
using Tamplate.Repository;

namespace Tamplate.Pages
{
    public class LoginPage
    {
        [FindsBy(How = How.Id, Using = "email")]
        private IWebElement emailAddressTextField;

        [FindsBy(How = How.Id, Using = "pass")]
        private IWebElement passwordTextField;

        [FindsBy(How = How.CssSelector, Using = "input[type='submit']")]
        private IWebElement logInButton;

       

        public void LogInAsLastRegisteredUser( )
        {                  
            emailAddressTextField.SendKeys(ObjectRepository.Config.GetUsername());
            passwordTextField.SendKeys(ObjectRepository.Config.GetPassword());
            logInButton.Click();
        }

        private void LogIn(User user)
        {
            emailAddressTextField.SendKeys(user.EmailAddress);
            
            passwordTextField.SendKeys(user.Password);

           
        }

        public enum LoginOptions
        {
            UseLastGeneratedPassword
        }

        public void Goto()
        {
            //Pages.TopNavigation.LogIn();
        }
    }
}
