﻿
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using Tamplate.Cookies;
using Tamplate.Repository;

namespace Tamplate.Pages
{
    public class HomePage
    {
        public void Goto()
        {
            //  Pages.TopNavigation.Home();
        }

        public bool IsAt()
        {
            return ObjectRepository.Driver.Title.Contains("Home");
        }
        public static void Cookie()
        {
            ReadOnlyCollection<Cookie> cooks = CookieManager.GetAllCookies();
            ObjectRepository.Driver.Quit();
            ObjectRepository.Init();
            ObjectRepository.Driver.Navigate().GoToUrl(ObjectRepository.Config.GetWebsite());
            foreach (var cook in cooks)
            {
                CookieManager.CokieAdd(cook);
            }
            
            ObjectRepository.Driver.Navigate().GoToUrl(ObjectRepository.Config.GetWebsite());

        }
    }
}
