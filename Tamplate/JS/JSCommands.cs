﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamplate.Repository;

namespace Tamplate.JS
{
    public class JSCommands
    {
        public static void JSClick(IWebElement elem)
        {
            ((IJavaScriptExecutor)ObjectRepository.Driver).ExecuteScript("arguments[0].click();",elem);
        }
    }
}

//((IJavaScriptExecutor) ObjectRepository.Driver).ExecuteScript("some script");
// o ze
//IJavaScriptExecutor js = ObjectRepository.Driver as IJavaScriptExecutor;
//js.ExecuteScript("OpenModalWindow('testfile.html')");