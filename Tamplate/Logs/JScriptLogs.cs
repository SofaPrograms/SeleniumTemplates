﻿using OpenQA.Selenium;
using Tamplate.Repository;

namespace Tamplate.Logs
{
    public class JScriptLogs
    {
        public static  void JSLogs()
        {
             
            FileWriter writer = FileWriter.GetInstance();
            foreach (var item in ObjectRepository.Driver.Manage().Logs.GetLog(LogType.Browser))
            {
                writer.WriteToLog(item.ToString());
            }  

        }
    }
}
