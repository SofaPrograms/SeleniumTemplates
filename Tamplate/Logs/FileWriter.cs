﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Tamplate.Repository;

namespace Tamplate.Logs
{
    public class FileWriter
    {

        private List<string> _logAssert = new List<string>();
        private static FileWriter _s_instance = null;
        private FileWriter() { }
        public static FileWriter GetInstance()
        {
            if (_s_instance == null)
            {
                _s_instance = new FileWriter();
            }
            return _s_instance;
        }


        public void WriteToLog(string text)
        {
            _logAssert.Add(text);
        }

        public void WriteToLog(List<string> messages)
        {
            _logAssert.AddRange(messages);
        }


        public void WriteToFile()
        {
            using (FileStream fs = new FileStream(Path.Combine( AssemblyDirectory, ObjectRepository.Config.GetLog()), FileMode.Open))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    foreach (var log in _logAssert)
                    {
                        sw.WriteLine(DateTime.Now.ToString() + "  :  " + log);
                    }
                }

            }
        }

        private static string AssemblyDirectory
        {
            get
            {
                //get the full location of the assembly with DaoTests in it
                string fullPath = System.Reflection.Assembly.GetAssembly(typeof(FileWriter)).Location;

                //get the folder that's in
                return Path.GetDirectoryName(fullPath);
            }
        }
    }
}
