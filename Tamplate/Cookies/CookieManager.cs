﻿using OpenQA.Selenium;
using Tamplate.Repository;
using System.Linq;
using System.Collections.ObjectModel;

namespace Tamplate.Cookies
{
    public class CookieManager
    {
        public static Cookie CookieGet(string arg0)
        {
                   
            return ObjectRepository.Driver.Manage().Cookies.GetCookieNamed(arg0);   //Return specific cookie according to name
        }

        public static ReadOnlyCollection<Cookie> GetAllCookies()
        {
            return ObjectRepository.Driver.Manage().Cookies.AllCookies;  // Return The List of all Cookies
        }
        public static void CokieAdd(Cookie coo)
        {
            ObjectRepository.Driver.Manage().Cookies.AddCookie(coo);    //Create and add the cookie


        }
        public static void CokieDel(Cookie coo)
        {
             ObjectRepository.Driver.Manage().Cookies.DeleteCookie(coo);  // Delete specific cookie according Name
        }
        public static void Cokie3(string arg0)
        {
            //ObjectRepository.Driver.Manage().Cookies.AllCookies();         // Return The List of all Cookies
            ObjectRepository.Driver.Manage().Cookies.GetCookieNamed(arg0);   //Return specific cookie according to name
            ObjectRepository.Driver.Manage().Cookies.DeleteCookieNamed(arg0);  // Delete specific cookie

         
            ObjectRepository.Driver.Manage().Cookies.DeleteAllCookies();   // Delete all cookies
        }
    }
}
