﻿ using OpenQA.Selenium.Support.PageObjects;
using Tamplate.Pages;

namespace Tamplate.Repository
{
   public static  class PageRepository
    {
        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(ObjectRepository.Driver, page);
            return page;
        }

      

        public static HomePage Home
        {
            get { return GetPage<HomePage>(); }
        }


        public static LoginPage Login
        {
            get { return GetPage<LoginPage>(); }
        }


    }

}
