﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamplate.Configurations;

namespace Tamplate.Repository
{
    class ObjectRepository
    {
        public static IWebDriver Driver { get; private set; }
        public static AppConfigReader Config { get; private set; }

        private static IWebDriver MakeWebDriver()
        {
            return DriverFactory.CreateWebdriver();
        }

        public static void Init()
        {
            Config = new AppConfigReader();
            Driver = MakeWebDriver();

        }
    }
}
