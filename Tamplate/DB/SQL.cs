﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tamplate.DB
{
    //table columns
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
        public bool InTheList { get; set; }
    }
    public class SQL
    {
        private string connectionString = @"Data Source = localhost\SQLEXPRESS; Initial Catalog = SofaDB; Integrated Security = True";

        public SQL()
        {


        }
        public void Add(Product entity)
        {
            var query = "INSERT INTO ProductsInList (Name,Category,Price,InTheList)   VALUES (@Name, @Category,@Price,@InTheList)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                command.Parameters.AddWithValue("@Name", entity.Name);
                command.Parameters.AddWithValue("@Category", entity.Category);
                command.Parameters.AddWithValue("@Price", entity.Price);
                command.Parameters.AddWithValue("@InTheList", entity.InTheList);

                foreach (IDataParameter param in command.Parameters)
                {
                    if (param.Value == null) param.Value = "";
                }

                command.ExecuteNonQuery();


                connection.Close();
            }
        }
        public void Delete(Product entity)
        {
            var query = "DELETE FROM ProductsInList WHERE Id=@Id";//WHERE??
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Id", entity.Id);
                command.Parameters.AddWithValue("@Name", entity.Name);
                command.Parameters.AddWithValue("@Category", entity.Category);
                command.Parameters.AddWithValue("@Price", entity.Price);
                command.Parameters.AddWithValue("@InTheList", entity.InTheList);

                foreach (IDataParameter param in command.Parameters)
                {
                    if (param.Value == null) param.Value = "";
                }

                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        public void Update(Product entity)
        {

            if (entity.Category == null && entity.Price == 0 && entity.Name == null)
            {
                var query = "UPDATE ProductsInList SET   InTheList=@InTheList WHERE Id=@Id";//WHERE??

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@Id", entity.Id);
                    // command.Parameters.AddWithValue("@Name", entity.Name);
                    //command.Parameters.AddWithValue("@Category", entity.Category);
                    command.Parameters.AddWithValue("@Price", entity.Price);
                    command.Parameters.AddWithValue("@InTheList", entity.InTheList);

                    //foreach (IDataParameter param in command.Parameters)
                    //{
                    //    if (param.Value == null) param.Value = "";
                    //}

                    command.ExecuteNonQuery();

                    connection.Close();
                }
            }
        }

        public IEnumerable<Product> GetAllProducts()
        {
            //var connectionString = @"Data Source = localhost\SQLEXPRESS; Initial Catalog = SofaDB; Integrated Security = True";

            var query = "SELECT  *  FROM ProductsInList";
            List<Product> products = new List<Product>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    products.Add(new Product()
                    {
                        Id = (int)reader["Id"],
                        Name = (string)reader["Name"],
                        Category = (string)reader["Category"],
                        InTheList = (bool)reader["InTheList"]
                    });

                }
                reader.Close();

                connection.Close();
            }
            return products;
        }

    }
}
