﻿using NUnit.Framework;
using Tamplate.Logs;
using Tamplate.Repository;

namespace Tamplate.Tests
{
    [SetUpFixture]
    public class PreTest
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            // inits
            ObjectRepository.Init();
            ObjectRepository.Driver.Navigate().GoToUrl(ObjectRepository.Config.GetWebsite());
        }

        [OneTimeTearDown]
        public void kill()
        {
            FileWriter writer = FileWriter.GetInstance();
            writer.WriteToFile();
            ObjectRepository.Driver.Close();
            ObjectRepository.Driver.Quit();
        }
    }
}
