﻿using NUnit.Framework;
using Tamplate.Repository;

namespace Tamplate.Tests
{
    [TestFixture, Category("name")]
    [Order(1)]
    public class LogIn
    {
        [Test]
        public void TestLogIn()
        {
            PageRepository.Login.LogInAsLastRegisteredUser();
        }
    }
}
